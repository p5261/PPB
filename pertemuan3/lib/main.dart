import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Latihan ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ScrollingScreen(),
    );
  }
}

class ScrollingScreen extends StatelessWidget {
  final List<String> nameList = ["Yoga","Djahwa","Julian","Mukmin","Andika","Ayu"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView'),
      ),
      body: ListView(
        physics: BouncingScrollPhysics(),
        children:
        nameList.map((name) {
            return Container(
              margin: EdgeInsets.all(5),
              height: 150,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage('https://media.istockphoto.com/photos/abstract-wavy-object-picture-id1198271727?b=1&k=20&m=1198271727&s=170667a&w=0&h=b626WM5c-lq9g_yGyD0vgufb4LQRX9UgYNWPaNUVses='),
                  fit: BoxFit.fill
                  ),
                color: Colors.grey,
                border: Border.all(
                  color: Colors.black,
                )
              ),
              child: Center(
                child: Text('$name'),
              ),
            );
        }).toList(),
      ),
    );
  }
}